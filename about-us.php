<?
$page_config = array(
    'title' => 'About us',
    'url' => '/about-us.php',
);
?>
<?include 'tpl/head_html.php';?>
<?include 'tpl/header.php';?>
<?$getTeamData = Manaklase::getTeam();
$team_img_route="assets/images/team/";?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<?if(!empty($getTeamData)){?>
<section class="our-webcoderskull padding-lg">
    <div class="container about-us">
        <div class="row heading heading-icon">
            <h2>Our Team</h2>
        </div>

        <ul class="row">
            <? foreach($getTeamData as $teamData){?>
            <li class="col-12 col-md-6 col-lg-3">
                <div class="cnt-block equal-height" style="height: 349px;">
                    <?$fullTeamImg=$team_img_route.$teamData['img'];?>
                    <img src="<?=$fullTeamImg?>" class="img-responsive" alt="">
                    <h3><a href=""><?=$teamData['name']?></a></h3>
                    <p><?=$teamData['about']?></p>
                    <ul class="follow-us clearfix">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </li>
            <?}?>
        </ul>
    </div>
</section>
<?}?>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>
