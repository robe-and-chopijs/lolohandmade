<?
$page_config = array(
    'title' => 'Library',
    'url' => '/library.php',
);
?>
<?include 'tpl/head_html.php';?>
<?include 'tpl/header.php';?>
<?$getLibraryData = Manaklase::getLibraryData();?>
<div class="library-class">
    <div class="container">
        <div class="row">
            <div class="col-md-12 upload-library pt-5">
                <a name="upload" id="uplaoad" class="btn btn-primary mt-3" href="library-input.php"
                role="button">Upload</a>
            </div>
        </div>
        <?if(!empty($getLibraryData)){?>
            <div class="row">
                <div class="col-md-12">
                    <section class="pt-5 pb-">
                        <table class="lib_table pt-3">
                            <tr>
                                <th>ID</th>
                                <th>Artist</th>
                                <th>Song</th>
                                <th>Genre</th>
                                <th style="width: 100px">Options</th>
                            </tr>
                            <?foreach($getLibraryData as $librarydata) {?>
                            <tr>
                                <td>#<?=$librarydata['id_library'];?></td>
                                <td><?=$librarydata['artist'];?></td>
                                <td><a href="#"><?=$librarydata['song'];?></a></td>
                                <td><?=$librarydata['genre'];?></td>
                                <td style=""><a name="edit" id="edit" class="btn btn-primary" href="/edit_library.php?edit_song=<?=$librarydata['id_library'];?>"
                                       role="button">EDIT</a>
                                </td>
                                <?}?>
                            </tr>
                        </table>
                    </section>
                </div>
            </div>
        <?}?>
    </div>
</div>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>