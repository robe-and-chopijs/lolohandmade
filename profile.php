<?
$page_config = array(
    'title' => 'Profile',
    'url' => '/profile.php',
);
?>
<? include 'tpl/head_html.php';?>
<? include 'tpl/header.php';?>
    <div class="container">
        <div class="row">
            <div class="card mt-4 mb-3 profile-card">
                <h2 class="text-monospace">Profile</h2>
                <img  style="height: auto; width: 25%"  src="/data/profile_pics/<?=$_SESSION['user_data']['img']?>" onerror="this.onerror=null; this.src='/data/profile_pics/default.jpg'" alt="">
                <p class="text-monospace">Name: <?=$_SESSION['user_data']['name'];?></p>
                <p class="text-monospace">e-mail: <?=$_SESSION['user_data']['email'];?></p>
                <p class="text-monospace">Status: <?=($_SESSION['user_data']['admin']==1 ? "Administrators" : "Vienkāršs lietotājs");?></p>
                <?if(!empty($_SESSION['user_data']['age'])){?>
                    <p class="text-monospace">Age: <?=$_SESSION['user_data']['age'];?></p>
                <?}?>
                <a style="width: 240px" href="user_data_edit.php" class="btn btn-primary">Edit Your User Data</a>
            </div>
        </div>
    </div>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>