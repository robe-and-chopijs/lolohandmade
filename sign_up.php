<?
$page_config = array(
    'title' => 'Sign up',
    'url' => '/sign_up.php',
);
?>
<?include 'tpl/head_html.php';?>
<?include 'tpl/header.php';?>
<?
if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['save_acc'])){
    $fullname = trim($_POST['name']);
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $c_password = trim($_POST['c_password']);
    $password_hash = password_hash($password, PASSWORD_BCRYPT);
    $error = '';
    $email_list = Manaklase::selectUserByEmail($email);
    if(!empty($email_list)){
        $error = 'The email is already registered!';
    }else{
        if(strlen($password) < 6 ){
            $error = 'Password must have atleast 6 characters.';
        }
        if(empty($c_password)){
            $error = 'Please confirm your password.';
        }else{
            if(empty($error) && ($password != $c_password)){
                $error = 'Passwords did not match.';
            }
        }
        if(empty($error)){
            $insertQuery = Manaklase::insertUser($fullname, $email, $password_hash);
            if(!empty($insertQuery)){
                $_SESSION['ok'] = 'Dati veiksmīgi ielikti!';
            }else{
                $error = (!empty($_SESSION['mysql_error']) ? $_SESSION['mysql_error'] : 'Something went wrong!');
            }
        }
    }
}
?>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2 class="pt-4 pt-2">Register</h2>
                <p>Please fill this form to create an account. :)</p>
                <?if(!empty($error)){?>
                    <div class="alert alert-danger" role="alert">
                        <strong><?=$error;?></strong>
                    </div>
                <?}?>
                <form action="" method="post">
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" name="name" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="c_password" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <button style="margin-bottom: 150px" type="submit" name="save_acc" id="save_acc" value="<?=date('ymdhis')?>" class="btn btn-success">Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>