<?
$page_config = array(
    'title' => 'News edit',
    'url' => '/edit_news.php',
);
?>
<? include 'tpl/head_html.php';?>
<? include 'tpl/header.php';?>
<div class="container">
    <div class="row">
        <div class="col-md-12 pt-5">
            <h2 class="pb-2">Edit an article</h2>
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" value="" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="cont">Content</label>
                    <textarea name="cont" id="cont" cols="30" rows="10" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit" name="update_song" id="update_song" value="<?=date('ymdhis')?>" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>