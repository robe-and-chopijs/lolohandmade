<?
$page_config = array(
    'url' => '/',
);
?>
<?include 'tpl/head_html.php';?>
<?include 'tpl/header.php';?>

                <br>
            <div class="container">
                <div class="row carousel-width">
                    <div class="col">
                        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="/assets/images/slideshow/image1.jpg" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Music for everyone</h5>
                                        <p>Come and learn with us</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="/assets/images/slideshow/image2.jpg" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Teach others</h5>
                                        <p>Submit your own songs and exercises</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="/assets/images/slideshow/image3.jpg" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Become a master</h5>
                                        <p>Learn enough to call yourself a master</p>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row info-cards">
                   <div class="col-md-4">
                       <div class="card-index">
                           <img src="/assets/images/info-box/image1.jpg" class="card-img-top" alt="...">
                           <div class="card-body">
                               <h5 class="card-title">Learn to play here</h5>
                               <p class="card-text">You can find a lot of interesting and different stuff here.</p>
                               <a href="library.php" class="btn btn-primary">Learn now</a>
                           </div>
                       </div>
                   </div>
                    <div class="col-md-4">
                        <div class="card-index">
                            <img src="/assets/images/info-box/image2.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Upload here</h5>
                                <p class="card-text">Upload your own creations here</p>
                                <a href="library-input.php" class="btn btn-primary">Inspire others</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card-index">
                            <img src="/assets/images/info-box/image3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Our team</h5>
                                <p class="card-text">Check out our team and all the stuff we do</p>
                                <a href="about-us.php" class="btn btn-primary">Learn about us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <br><br>

<?include 'tpl/footer.php';?>
<?include 'tpl/foot_html.php';?>