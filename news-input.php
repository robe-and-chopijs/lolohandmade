<?
$page_config = array(
    'title' => 'Nws input',
    'url' => '/news-input.php',
);
?>
<?include 'tpl/head_html.php';?>
<?include 'tpl/header.php';?>
<?if(!empty($_POST['save_news'])){
    if(empty($_POST['title']) || empty($_POST['cont'])){
    $error = 'Ludzu aizpildiet visus laukus!';
    }
    if(!empty($_FILES['image']) && $_FILES['image']['error'] == 0){
        if($_FILES['image']['type'] == 'image/jpeg' || $_FILES['image']['type'] == 'image/gif' || $_FILES['image']['type'] == 'image/png'){
            $upload_dir = ROOT.'data/news_photos/';
            if (!file_exists($upload_dir)) {
                mkdir($upload_dir, 0777, true);
            }
            $new_file_name = date('YmdHis').'_newsArticle.'.Manaklase::setImageType($_FILES['image']['type']);
            $image_full_path_and_name = $upload_dir.$new_file_name;
            if(move_uploaded_file($_FILES['image']['tmp_name'], $image_full_path_and_name)){ #Kopējam failu no paigaidu TMP lokācijas uz mūsu dirkciju ar pilnu faila nosaukumus
                $uploaded_image_name = $new_file_name;
            }else{
                $error = 'Nesanāca augšupielādēt failu!';
            }
        }else{
            $error = 'Šāds bildes formats netiek atbalstīts!';
        }
    }
    if(empty($error)){
        $p = $_POST;
        if(!empty($uploaded_image_name)){
            $p['image'] = $uploaded_image_name;
        }
        $addDataToDB = Manaklase::addNewsArticle($p);
        if(!empty($addDataToDB)){
            $_SESSION['ok'] = 'Dati veiksmīgi ielikti!';
            #header('Location:/team.php'); exit();
        }else{
            $error = (!empty($_SESSION['mysql_error']) ? $_SESSION['mysql_error'] : 'Ir notikusi nezināma kļūda!');
        }
    }

}?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 pt-5">
                <?if(!empty($error)){?>
                    <div class="col-md-12">
                        <div class="alert alert-danger"><?=$error;?></div>
                    </div>
                <?}?>
                <?if(!empty($_SESSION['ok'])){?>
                    <div class="col-md-12">
                        <div class="alert alert-success"><?=$_SESSION['ok'];?></div>
                    </div>
                    <?
                    $_SESSION['ok'] = '';
                    unset($_SESSION['ok']);
                    ?>
                <?}?>
                <?if(!empty($_SESSION['mysql_error'])){?>
                    <div class="col-md-12">
                        <div class="alert alert-danger"><?=$_SESSION['mysql_error'];?></div>
                    </div>
                    <?
                    $_SESSION['mysql_error'] = '';
                    unset($_SESSION['mysql_error']);
                    ?>
                <?}?>
                <h2 class="pb-2">Insert a news article</h2>
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" value="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="cont">Content</label>
                        <textarea name="cont" id="cont" cols="30" rows="13" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="image">Insert an image</label>
                        <input type="file" name="image" id="image">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="save_news" id="save_news" value="<?=date('ymdhis')?>" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>