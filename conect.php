<?
session_start();
ob_implicit_flush(true);
ini_set('date.timezone', 'Europe/Riga');
ini_set('error_reporting', E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_DEPRECATED);
error_reporting(E_ALL ^ E_STRICT);
define('ROOT', $_SERVER['DOCUMENT_ROOT'] .'/');
define('USER_ID', (!empty($_SESSION["user_id"]) ? $_SESSION["user_id"] : false));
define('ADMIN', (USER_ID && $_SESSION['user_data']['admin']== 1 ? $_SESSION["user_id"] : false));
if(isset($_GET['logout'])){
    session_destroy();
    unset($_SESSION);
    header('Location:/'); exit();
}