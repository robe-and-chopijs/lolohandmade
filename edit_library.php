<?
$page_config = array(
    'title' => 'Library edit',
    'url' => '/edit_library.php',
);
?>
<? include 'tpl/head_html.php';?>
<? include 'tpl/header.php';?>
<?
if($_SESSION[ADMIN] = true){
    $song_id = $_GET['edit_song'];
    if(isset($_POST['delete_song'])){
        $delete = Manaklase::deleteLibraryPost($song_id);
        if(!empty($delete)){
            $_SESSION['ok'] = 'Ieraksts ir dzēsts!';
            /*header('Location:/team.php?editTeamMember='.$libraryID);*/
            exit();
        }else{
            $error = 'Ir notikusi nezināma kļūda!';
        }
    }
    if(!empty($_POST['update_song'])){
        if(empty($_POST['song']) || empty($_POST['content']) || empty($_POST['artist'])  || empty($_POST['genre'])){
        $error = 'Kļūda: nav aizpildīti visi lauki!';
        }
        if(empty($error)){
            $p = $_POST;
            $p['id_library'] = $song_id;
            $update = Manaklase::updateLibrary($p);
            if(!empty($update)){
                $_SESSION['ok'] = 'Dati veiksmīgi atjaunoti!';
                /*header('Location:/libary.php');*/
                exit();
            }else{
                $error = (!empty($_SESSION['mysql_error']) ? $_SESSION['mysql_error'] : 'Ir notikusi nezināma kļūda!');
            }
        }
    }
    $song = Manaklase::getSongByID($song_id);
    if(!empty($song)){
    ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5">
                    <h2 class="pb-2">Edit a song</h2>
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="artist">Artist</label>
                            <input type="text" name="artist" id="artist" value="<?=htmlentities($song['artist']);?>" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="title">Song</label>
                            <input type="text" name="song" id="song" value="<?=htmlentities($song['song']);?>" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="cont">Content</label>
                            <textarea name="content" id="content" cols="30" rows="10" class="form-control"><?=htmlentities($song['content']);?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="genre">Choose a genre:</label>
                            <?if(!empty($select_genre)){?>
                                <select name="genre" id="genre">
                                    <?foreach($select_genre as $genre){?>
                                        <option value="<?=$genre['genre'];?>"<?=(!empty($song['genre']) && $song['genre'] == $genre['genre'] ? ' SELECTED'  : '');?>><?=$genre['genre'];?></option>
                                    <?}?>
                                </select>
                            <?}?>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="update_song" id="update_song" value="<?=date('ymdhis')?>" class="btn btn-success">Update</button>
                            <button type="submit" name="delete_song" id="delete_song" value="<?=date('ymdhis')?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this library post?');">Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?}else{?>
        <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    Šāda ieraksta nav mūsu sistēmā!
                </div>
            </div>
        </div>
    <?}
}else{
    $error = 'Jums nav piekļuves šai sadaļai.';
    if(!empty($error)){?>
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                    <div class="alert alert-danger" role="alert">
                        <strong><?=$error;?></strong>
                    </div>
                </div>
            </div>
        </div>
    <?}
}?>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>