<?php
class Manaklase
{
    public static function shortenText($text, $count){
        return strlen($text) > $count ? substr($text,0,$count)." ..." : $text;
    }
    public static function getLibraryData(){
        $q = query("SELECT * FROM `library` ORDER BY `id_library` ASC ");
        while($d = mysqli_fetch_assoc($q)) {
            $data[$d['id_library']] = $d;
        }
        if(!empty($data)){
            return $data;
        }
    }
    public static function addLibrary($post){
        $today = date('Y-m-d H:i:s');
        if(query("INSERT INTO `library` SET
            `artist` = '".mysqli_real_escape_string(db(), $post['artist'])."',
            `song` = '".mysqli_real_escape_string(db(), $post['title'])."',
            `content` = '".mysqli_real_escape_string(db(), $post['cont'])."',
            `genre` = '".mysqli_real_escape_string(db(), $post['genre'])."'
        ") && mysqli_affected_rows(db())){
            $id = mysqli_insert_id(db());
            return $id;
        }else{
            $_SESSION['mysql_error'] = mysqli_error(db());
        }
    }
    public static function updateLibrary($post){
        $today = date('Y-m-d H:i:s');
        if(query("UPDATE `library` SET
            `artist` = '".mysqli_real_escape_string(db(), $post['artist'])."',
            `song` = '".mysqli_real_escape_string(db(), $post['song'])."',
            `content` = '".mysqli_real_escape_string(db(), $post['content'])."',
            `genre` = '".mysqli_real_escape_string(db(), $post['genre'])."'
            WHERE `id_library` = '".(int) $post['id_library']."'
        ") && mysqli_affected_rows(db())){
            return $post['id_library'];
        }else{
            $_SESSION['mysql_error'] = mysqli_error(db());
        }
    }
    public static function deleteLibraryPost($id){
        if(query("DELETE FROM `library` WHERE `id_library` = '".(int) $id."'")
            && mysqli_affected_rows(db())){
                return 'OK';
        }else{
            $_SESSION['mysql_error'] = mysqli_error(db());
        }
    }
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public static function getSongByID($id){
        $q = query("SELECT * FROM `library` WHERE `id_library` = '".(int) $id."'");
        return mysqli_fetch_assoc($q);
    }
    public static function getTeam(){
        $q = query("SELECT * FROM `team` ORDER BY `id_team` ASC ");
        while($d = mysqli_fetch_assoc($q)) {
            $data[$d['id_team']] = $d;
        }
        if(!empty($data)){
            return $data;
        }
    }
    public static function setImageType($type){
        if($type == 'image/jpeg'){
            return 'jpg';
        }elseif($type == 'image/gif'){
            return 'gif';
        }elseif($type == 'image/png'){
            return 'png';
        }else{
            return 'unknown';
        }
    }
    public static function addNewsArticle($post){
        $today = date('Y-m-d H:i:s');
        if(query("INSERT INTO `news` SET
            `title` = '".mysqli_real_escape_string(db(), $post['title'])."',
            `content` = '".mysqli_real_escape_string(db(), $post['cont'])."',
            `img` = '".mysqli_real_escape_string(db(), $post['image'])."',
            `date` = '".mysqli_real_escape_string(db(), $today)."'
        ") && mysqli_affected_rows(db())){
            $id = mysqli_insert_id(db());
            return $id;
        }else{
            $_SESSION['mysql_error'] = mysqli_error(db());
        }
    }
    public static function getNewsData(){
        $q = query("SELECT * FROM `news` ORDER BY `id_news` ASC ");
        while($d = mysqli_fetch_assoc($q)) {
            $data[$d['id_news']] = $d;
        }
        if(!empty($data)){
            return $data;
        }
    }
    public static function selectUserByEmail($email){
        $q = query("SELECT * FROM `users` WHERE `email` = '".mysqli_real_escape_string(db(), $email)."'");
        return mysqli_fetch_assoc($q);
    }
    public static function insertUser($fullname, $email, $password_hash){
        if(query("INSERT INTO `users` SET
            `name` = '".mysqli_real_escape_string(db(), $fullname)."',
            `email` = '".mysqli_real_escape_string(db(), $email)."',
            `password` = '".mysqli_real_escape_string(db(), $password_hash)."'
        ") && mysqli_affected_rows(db())){
            $id = mysqli_insert_id(db());
            return $id;
        }else{
            $_SESSION['mysql_error'] = mysqli_error(db());
        }
    }
    public static function setAge($age, $id_user ){
        if(query("UPDATE `users` SET
            `age` = '".mysqli_real_escape_string(db(), $age)."'
       WHERE `id_user` = $id_user") && mysqli_affected_rows(db())){
            $id = mysqli_insert_id(db());
            return $id_user;
        }else{
            $_SESSION['mysql_error'] = mysqli_error(db());
        }
    }
    public static function updateUser($post, $password_hash){
        if(query("UPDATE `users` SET
            `name` = '".mysqli_real_escape_string(db(), $post['name'])."',
            `password` = '".mysqli_real_escape_string(db(), $password_hash)."',
            `email` = '".mysqli_real_escape_string(db(), $post['email'])."'
            `profile_pic` = '".mysqli_real_escape_string(db(), $post['image'])."',
            `age` = '".mysqli_real_escape_string(db(), $post['age'])."'
            WHERE `id_user` = '".(int) $post['id_user']."'
        ") && mysqli_affected_rows(db())){
            return $post['id_user'];
        }else{
            $_SESSION['mysql_error'] = mysqli_error(db());
        }
    }
}