<!-- Modal -->
<?
if(!empty($_POST['login'])){
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $error = "";
    if(empty($email)){
        $error = "Please enter your email.";
    }
    if(empty($password)){
        $error = "Please enter your password.";
    }
    if(empty($error)){
        $user = Manaklase::selectUserByEmail($email);
        if(!empty($user)){
            if(password_verify($password, $user['password'])){
                $_SESSION["user_id"] = $user['id_user'];
                $_SESSION["user_data"] = $user;
            }else{
                $error = 'Password is not valid.';
            }
        }else{
            $error = 'Email is not valid.';
        }
    }
}
?>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">User login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="sidelog">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group flex-nowrap">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="addon-wrapping">@</span>
                                    </div>
                                    <input type="text" class="form-control" name="email" placeholder="e-mail" aria-label="e-mail" aria-describedby="addon-wrapping">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group flex-nowrap">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="addon-wrapping"></span>
                                    </div>
                                    <input type="password" class="form-control" name="password" placeholder="Password" aria-label="Password" aria-describedby="addon-wrapping">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group flex-nowrap">
                                    <p>Dont have an account? <a href="sign_up.php">Sign up</a>.</p>
                                </div>
                                <?if(!empty($error)){?>
                                    <div class="alert alert-danger" role="alert">
                                        <strong><?=$error;?></strong>
                                    </div>
                                <?}?>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="login" value="1" class="btn btn-primary">Login</button>
                </div>
            </div>
        </form>
    </div>
</div>
<footer class="color-max-width foot-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="align-center left-footer"><?=$lapas_iestatijumi['lapas_apraksts']?></div>
            </div>
            <div class="col-md-4 ">
                <div class="align-center"><img width="55%" height="auto" src="<?=$lapas_iestatijumi['logo'];?>" alt=""></div>
            </div>
            <div class="col-md-4 ">
                <div class="align-center right-footer"><?=$lapas_iestatijumi['copyright']?></div>
            </div>
        </div>
    </div>
</footer>
