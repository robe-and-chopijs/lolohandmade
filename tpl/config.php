<?
$lapas_iestatijumi = array(
'lapas_nosaukums' => 'Robčika mūzika.lv',
'lapas_apraksts' => 'Mūzika jums, bauda mums',
'favicon' => '',
'copyright' => 'RobKob © '.date('Y').'. Visas tiesības aizsargātas',
'logo' => '/assets/images/logo1.png',
);
$main_nav = array();
$main_nav[] = array(
    'link' => '/',
    'title' => 'Home',
);
$main_nav[] = array(
    'link' => '/about-us.php',
    'title' => 'About us',
);
$main_nav[] = array(
    'link' => '/library.php',
    'title' => 'Library',
);
$main_nav[] = array(
    'link' => '/news.php',
    'title' => 'News',
);
if(USER_ID){
    $main_nav[] = array(
        'link' => '/profile.php',
        'title' => 'Profile',
    );
}
$news = array();
$news[] = array(
    'title' => 'Title1',
    'content' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. ',
    'date' => date('Y.M.d'),
    'img' => 'news_1.jpg',

);
$news[] = array(
    'title' => 'Title2',
    'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse laoreet hendrerit sapien pretium finibus. Integer tellus lorem, ullamcorper quis erat ac, lobortis lobortis tellus. Donec ullamcorper rutrum dui non aliquet. Curabitur sit amet maximus neque, a mattis eros. Sed felis justo, venenatis in condimentum at, congue sed nunc. Vestibulum auctor nibh sed arcu dapibus vehicula. Vivamus eget ante purus. Suspendisse potenti. Aenean aliquam enim eros, nec varius nisi sodales a. Nullam sit amet eleifend neque. Nullam elementum consequat nunc, eu commodo arcu vulputate eu. Quisque commodo scelerisque sollicitudin. Etiam blandit maximus augue, nec gravida metus maximus et. Donec elementum, ex id ullamcorper volutpat, nibh metus mollis mi, in fermentum ante massa eu nisi.',
    'date' => date('Y.M.d'),
    'img' => 'news_2.jpg',
);
$news[] = array(
    'title' => 'Title3',
    'content' => 'Etiam sollicitudin in odio sed sollicitudin. Cras non scelerisque dolor. Ut feugiat maximus felis in convallis. Nam eu lorem pulvinar, ultrices sapien quis, commodo quam. Nam gravida sed ante ornare ornare. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc cursus mollis posuere. Suspendisse condimentum erat ac dolor accumsan, scelerisque gravida est blandit. Quisque porttitor rhoncus neque, lobortis mollis sem lacinia sit amet. Vivamus risus dui, egestas eu imperdiet vitae, porta a metus. In porttitor ultrices blandit. Quisque bibendum congue ligula non fermentum. Quisque rhoncus quam et tortor imperdiet, vel finibus elit dapibus. Etiam ac leo augue. Nam eget nisi in nulla ornare interdum vitae nec odio. Pellentesque maximus scelerisque odio.',
    'date' => date('Y.M.d'),
    'img' => 'news_3.jpg',
);
$news[] = array(
    'title' => 'Title4',
    'content' => 'Get started with Bootstrap, the world’s most popular framework for building responsive, mobile-first sites, with jsDelivr and a template starter page.',
    'date' => date('Y.M.d'),
    'img' => 'news_4.jpg',
);
$news[] = array(
    'title' => 'Title5',
    'content' => 'Get started witadasdasdasdassdwadadawh Bootstrap, the world’s most popular framework for building responsive, mobile-first sites, with jsDelivr and a template starter page.',
    'date' => date('Y.M.d'),
    'img' => '',
);
$side_nav = array();
$side_nav[] = array(
    'artist' => 'Nirvana',
    'song' => 'Come as you are',
);
$side_nav[] = array(
    'artist' => 'Radiohead',
    'song' => 'Creep',
);
$side_nav[] = array(
    'artist' => 'John Legend',
    'song' => 'All Of Me',
);
$side_nav[] = array(
    'artist' => 'Pink Floyd',
    'song' => 'Wish You Were Here',
);
$side_nav[] = array(
    'artist' => 'Metallica',
    'song' => 'Nothing Else Matters',
);
$side_nav[] = array(
    'artist' => 'Fleetwood Mac',
    'song' => 'The Chain',
);
$side_nav[] = array(
    'artist' => 'John Denver',
    'song' => 'Take Me Home Country Roads',
);
$side_nav[] = array(
    'artist' => 'Creedence Clearwater Revival',
    'song' => 'Have You Ever Seen The Rain',
);
$side_nav[] = array(
    'artist' => 'Tumsa',
    'song' => 'Bet Maijpuķītēm Jāpaliek',
);
$side_nav[] = array(
    'artist' => 'Prāta Vētra',
    'song' => 'Četri Krasti',
);
$select_genre = array();
$select_genre[] = array(
    'title' => '---Select---',
    'genre' => '---Select---',
);
$select_genre[] = array(
    'title' => 'Rock',
    'genre' => 'Rock',
);
$select_genre[] = array(
    'title' => 'Metal',
    'genre' => 'Metal',
);
$select_genre[] = array(
    'title' => 'Folk',
    'genre' => 'Folk',
);
$select_genre[] = array(
    'title' => 'Pop',
    'genre' => 'Pop',
);
$select_genre[] = array(
    'title' => 'Country',
    'genre' => 'Country',
);
$select_genre[] = array(
    'title' => 'RnB',
    'genre' => 'RnB',
);