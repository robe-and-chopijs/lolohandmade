<header>
    <div class="manas-lapas-klase">
        <div class="container sticky-top">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg navbar-dark nav-bg">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a href="/">
                            <img height="90" width="90" src=<?=$lapas_iestatijumi['logo'];?>>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                            <?if(isset($main_nav)){?>
                                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                                    <?foreach($main_nav as $nav_item){?>
                                        <?
                                        $nav_bar_active_link = false;
                                        if($nav_item['link'] == $page_config['url']){
                                            $nav_bar_active_link = true;
                                        }
                                        ?>
                                        <li class="nav-item <?=(!empty($nav_bar_active_link) ? 'active' : '');?>">
                                            <a class="nav-link" href="<?=$nav_item['link'];?>"><?=$nav_item['title'];?><?=(isset($nav_bar_active_link) ? ' <span class="sr-only">(current)</span>' : '');?></a>
                                        </li>
                                    <?}?>

                            <?}?>
                                <?if(USER_ID){?>
                                    <li class="nav-item <?=(!empty($nav_bar_active_link) ? 'active' : '');?>">
                                        
                                    </li><li class="nav-item">
                                        <a class="btn" href="/?logout" ">Logout</a>
                                    </li>
                                <?}else{?>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal">Login</a>
                                    </li>
                                <?}?>
                            </ul>
                            <form class="form-inline my-2 my-lg-0 ">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                                <button class="btn btn-outline-light my-2 my-sm-0" type="">Search</button>
                            </form>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
