<?
$page_config = array(
    'title' => 'Library input',
    'url' => '/library-input.php',
);
?>
<?include 'tpl/head_html.php';?>
<?include 'tpl/header.php';?>
<?
if(!empty($_POST['save_song'])){
    if(empty($_POST['title']) || empty($_POST['artist']) || empty($_POST['cont'])){
        $error = 'Ludzu aizpildiet visus laukus!';
    }
    if(empty($error)){
        $p = $_POST;
        $addDataToDB = Manaklase::addLibrary($p);
        if(!empty($addDataToDB)){
            $_SESSION['ok'] = 'Dati veiksmīgi ielikti!';
            /*header('Location:/library.php'); exit();*/
        }else{
            $error = (!empty($_SESSION['mysql_error']) ? $_SESSION['mysql_error'] : 'Ir notikusi nezināma kļūda!');
        }
    }
}
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 pt-5">
                <?if(!empty($error)){?>
                    <div class="col-md-12">
                        <div class="alert alert-danger"><?=$error;?></div>
                    </div>
                <?}?>
                <?if(!empty($_SESSION['ok'])){?>
                    <div class="col-md-12">
                        <div class="alert alert-success"><?=$_SESSION['ok'];?></div>
                    </div>
                    <?
                    $_SESSION['ok'] = '';
                    unset($_SESSION['ok']);
                    ?>
                <?}?>
                <?if(!empty($_SESSION['mysql_error'])){?>
                    <div class="col-md-12">
                        <div class="alert alert-danger"><?=$_SESSION['mysql_error'];?></div>
                    </div>
                    <?
                    $_SESSION['mysql_error'] = '';
                    unset($_SESSION['mysql_error']);
                    ?>
                <?}?>
                <h2 class="pb-2">Insert song</h2>
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="artist">Artist</label>
                        <input type="text" name="artist" id="artist" value="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" value="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="cont">Content</label>
                        <textarea name="cont" id="cont" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="genre">Choose a genre:</label>
                        <?if(!empty($select_genre)){?>
                            <select name="genre" id="genre">
                                <?foreach($select_genre as $genre){?>
                                    <option value="<?=$genre['genre'];?>"><?=$genre['title'];?></option>
                                <?}?>
                            </select>
                        <?}?>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="save_song" id="save_song" value="<?=date('ymdhis')?>" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>