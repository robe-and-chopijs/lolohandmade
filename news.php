<?
$page_config = array(
    'title' => 'News',
    'url' => '/news.php',
);
?>
<?include 'tpl/head_html.php';?>
<?include 'tpl/header.php';?>
<?$getNews = Manaklase::getNewsData()?>
<div class="news-class">
    <div class="container mt-100 mt-60">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">Latest Blog &amp; News</h4>
                    <a name="edit" id="edit" class="btn btn-primary" href="/news-input.php"
                       role="button">Upload
                    </a>
                </div>
            </div><!--end col-->
        </div><!--end row-->
        <div class="row">
            <?if(!empty($getNews)){?>
                <?
                $news_pic_route = '/data/news_photos/';
                foreach($getNews as $news_items){
                ?>
                    <div class="col-lg-4 col-md-6 mt-4 pt-2 pb-5 ">
                        <div class="blog-post rounded border">
                            <div class="blog-img d-block overflow-hidden position-relative">
                                <img src="<?=$news_pic_route.$news_items['img']?>" class="img-fluid rounded-top" onerror="this.onerror=null; this.src='<?=$news_pic_route?>default.jpg'" alt="">
                                <div class="overlay rounded-top bg-dark"></div>
                                <div class="post-meta">
                                    <a href="#" class="text-light read-more">Read More <i class="mdi mdi-chevron-right"></i></a>
                                </div>
                            </div>
                            <div class="news-content content p-3">
                                <?if(!empty($news_items['title']) || !empty($news_items['content'])){?>
                                    <small class="text-muted p float-right"><?=$news_items['date']?></small>
                                    <?=(!empty($news_items['title']) ? '<h4 class="mt-2"><a href="javascript:void(0)" class="text-dark title">'.$news_items['title'].'</a></h4>' : '')?>
                                    <?=(!empty($news_items['content']) ? '<small class="mt-2">'.Manaklase::shortenText($news_items['content'],170).'</small>' : '')?>
                                <?}?>
                            </div>
                            <div class="news-btn">
                                <button  type="button" class="btn btn-primary">Edit</button>
                            </div>
                        </div>
                    </div>
                <?}?>
            <?}else{?>
            <div class="alert alert-info">"Atvainojiet, pašlaik nav ievieots neviens ieraksts"</div>
            <?}?>
        </div>
    </div>
</div>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>