<?
$page_config = array(
    'title' => 'User Data Edit',
    'url' => '/user_data_edit.php',
);
?>
<? include 'tpl/head_html.php';?>
<? include 'tpl/header.php';?>
<?
$user_id = $_SESSION['user_data']['id_user'];
if(isset($_POST['save_acc'])){
    if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['age'])){
        if(!empty($_POST['password'])){
            
        }
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h2 class="pt-2">Edit user data</h2>
            <p>Change data</p>
            <?if(!empty($error)){?>
                <div class="alert alert-danger" role="alert">
                    <strong><?=$error;?></strong>
                </div>
            <?}?>

            <form action="" method="post">
                <div class="form-group">
                    <label>Change Full Name</label>
                    <input type="text" value="<?=$_SESSION['user_data']['name']?>" name="name" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label>Change e-mail</label>
                    <input type="email" value="<?=$_SESSION['user_data']['email']?>" name="email" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label>Old Password</label>
                    <input type="password" name="old_password" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" name="c_password" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label>Enter Your Age</label>
                    <input type="text" name="age" class="form-control" required/>
                </div>
                <div class="form-group">
                    <input type="file" name="image" id="image">
                </div>
                <div class="form-group">
                    <button style="margin-bottom: 150px" type="submit" name="save_acc" id="save_acc" value="<?=date('ymdhis')?>" class="btn btn-success">Sign Up</button>
                </div>
            </form>
        </div>
    </div>
</div>
<? include 'tpl/footer.php';?>
<? include 'tpl/foot_html.php';?>